{-# LANGUAGE GeneralizedNewtypeDeriving #-}

import System.Console.Readline

import Data.Char (isSpace)
import Data.List
import Data.Maybe
import qualified Data.Set as S
import qualified Data.Map as M
import Text.Parsec
import Text.Parsec.String
import Control.Applicative hiding (many, (<|>), optional)
import Control.Monad.Error

import Debug.Trace

newtype Id = Id { unId :: Int }
    deriving (Eq, Num, Ord, Enum)

instance Show Id where
    show = show . unId

data Node = Node {
    n_id :: Id 
    , n_s :: String 
    , n_deps :: [Id]
    }
    deriving (Eq)

type Graph = [Node]

data GraphCmd = Insert String [Id] | Change Id String [Id]
    deriving (Show)

type GraphError = String

{- parsing stuff -}
endstrs = "([<\n"

trim = f . f where f = reverse . dropWhile isSpace

ident = spaces *> (Id . read <$> many1 digit)

ids = oneOf "([<" *> sepEndBy (ident <* spaces) (char ',') <* oneOf ">])"

insertCmd = Insert <$> 
    (char 'i' *> many1 (oneOf " \t") *> (trim <$> many (noneOf endstrs))) <*>
    (option [] ids <* eof)

changeCmd = Change <$> 
    (ident <* optional (char '.') <* many1 (oneOf " \t")) <*>
    (trim <$> many (noneOf endstrs)) <*> 
    (option [] ids <* eof)

graphCmd :: Parser GraphCmd
graphCmd = insertCmd <|> changeCmd
{- end parsing -}

graphGet :: Graph -> Id -> Maybe Node
graphGet g id = find ((== id) . n_id) g

graphGetF :: Graph -> Id -> Node
graphGetF g = fromJust . graphGet g

checkPreds :: Graph -> [Id] -> Either GraphError ()
checkPreds g ids = case (ids \\ map n_id g) of
    [] -> Right ()
    (x:_) -> Left $ "The id " ++ (show x) ++ " was not found."

editGraph :: Graph -> GraphCmd -> Either GraphError Graph

editGraph g (Insert t preds) = do
    checkPreds g preds
    return $ normalize $ g ++ [(Node freshid t preds)]
    where
    freshid | null g    = 1
            | otherwise = 1 + maximum [i | (Node i _ _) <- g]

editGraph g (Change id t preds) = do
    checkPreds g' preds
    return $ normalize $ g' ++ [(Node id t preds)]
    where
    g' = filter ((id /=) . n_id) g

tsort :: Graph -> Graph
tsort g = snd $ foldl' tsortvisit (S.empty, []) [i | Node i _ _ <- g]
    where
    tsortvisit :: (S.Set Id, Graph) -> Id -> (S.Set Id, Graph)
    tsortvisit (s, xs) i
        | i `S.member` s = (s, xs)
        | otherwise      = trace ("node " ++ show i ++ "\n") $
            let x@(Node _ _ deps) = graphGetF g i
                (s', xs')         = foldl' tsortvisit (S.insert i s, xs) deps 
            in (s', x:xs')

normalize :: Graph -> Graph
normalize = renumber . reverse . tsort

renumber :: Graph -> Graph
renumber g = map (substInNode substs) g
    where
    substs = M.fromList $ zip [i | (Node i _ _) <- g] [Id 1..]
    substInNode substs (Node i s deps) = 
        Node (substs M.! i) s (sort $ map (substs M.!) deps)

showGraph :: Graph -> String
showGraph = unlines . map showNode
    where
    showNode (Node i s deps) = 
        (show i) ++ ". " ++ s ++ 
        " [" ++ intercalate ", " (map show deps) ++ "]"

repl :: Graph -> IO ()
repl x = do
    maybeLine <- readline "% "
    case maybeLine of 
        Nothing     -> return () -- EOF / control-d
        Just "exit" -> return ()
        Just ""     -> repl x
        Just line -> do 
            addHistory line
            x' <- case (parse graphCmd "(input)" line) of
                Left e -> putStrLn (show e) >> return x
                Right c -> case (editGraph x c) of
                    Left e -> putStrLn e >> return x
                    Right g -> return g
            putStr (showGraph x')
            repl x'

main = repl []
